/**
 * board.h
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C)  2011  Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#ifndef BOARD_H
#define BOARD_H

#include "xmlpicross.h"
#include <QObject>

#include <QList>
#include <QVector>
#include <QDataStream>
#include <QPair>

class Board : public QObject
{
    Q_OBJECT
public:
    enum TileState {
        Empty=0,
        CheckedAsFilled,
        CheckedAsEmpty
    };
public:
    Board(QObject *parent=0);
    Board(const BoardXml &data, QObject *parent=0);

    BoardXml data() const;

    QVector< QVector<TileState> > board() const;
    QVector< QVector<bool> > picture() const;

    int rows() const;
    int columns() const;

    int maxRowHints() const;
    int maxColumnHints() const;

    QList<int> hintRow(int) const;
    QList<int> hintColumn(int) const;

    QVector<TileState> row(int) const;
    QVector<TileState> column(int) const;

    QPair<int, int> giveHint() const;

    TileState tileAt(int x, int y) const;

    /**
     * true if the CheckedAsFilled pieces equal the
     * actually filled pieces
     */
    bool isCompleted() const;

    void load(QDataStream &stream);
    void save(QDataStream &stream);

    QString title() const;
    void setTitle(const QString &title);

    QString author() const;
    void setAuthor(const QString &name);

public slots:
    void setTile(TileState state, int x, int y);
    void setPicture(const QVector< QVector<bool> > &pic);

signals:
    /**
     * Emitted when a tile changes.
     */
    void changed(Board::TileState state, int x, int y);
    /**
     * Emitted when the m_board equals the m_picture.
     */
    void completed();

    void newBoard();

protected:
    void computeHints();
    void computeHintRows();
    void computeHintColumns();
private:
    BoardXml m_data;

    QVector< QVector<TileState> > m_board;
    QVector< QList<int> > m_hintRows;
    QVector< QList<int> > m_hintColumns;
    int m_maxRowHints;
    int m_maxColumnHints;
    int m_tilesLeft;
};

#endif
