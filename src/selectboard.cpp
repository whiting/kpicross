/**
 * selectboard.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "selectboard.h"

#include <klocale.h>


SelectBoard::SelectBoard(const QList<BoardSetXml> &sets, QWidget *parent)
    : KDialog(parent),
      m_sets(sets)
{
    KDialog::setObjectName( "boarddialog" );
    KDialog::setModal(true);

    m_ui = new Ui::LevelSelectUi;
    m_ui->setupUi(mainWidget());

    fillTreeView();

    connect(m_ui->treeWidget, SIGNAL(itemActivated(QTreeWidgetItem*,int)),
            SLOT(boardActivated(QTreeWidgetItem*, int)));
}

BoardXml SelectBoard::selectedBoard() const
{
    if (m_currentBoard.name.isEmpty()) {
        QList<QTreeWidgetItem*> selected = m_ui->treeWidget->selectedItems();
        if (!selected.isEmpty())
            findBoardFromItem(selected.first());
    }
    return m_currentBoard;
}

void SelectBoard::fillTreeView()
{
    QList<QTreeWidgetItem *> items;

    foreach(BoardSetXml set, m_sets) {
        QTreeWidgetItem *current = new QTreeWidgetItem(
            (QTreeWidget*)0, QStringList(set.name));
        foreach(BoardXml board, set.boards) {
            QTreeWidgetItem *boardItem = new QTreeWidgetItem(current);
            boardItem->setText(0, board.name);
            boardItem->setText(1,
                               QString("%1x%2")
                               .arg(board.rows)
                               .arg(board.columns));
        }
        items.append(current);
    }
    m_ui->treeWidget->insertTopLevelItems(0, items);
}


void SelectBoard::boardActivated(QTreeWidgetItem *item, int column)
{
    Q_UNUSED(column);
    findBoardFromItem(item);
    accept();
}


void SelectBoard::findBoardFromItem(const QTreeWidgetItem *item) const
{
    QTreeWidgetItem *parent = item->parent();
    //top level
    if (!parent)
        return;

    foreach(BoardSetXml set, m_sets) {

        if (set.name == parent->text(0)) {
            foreach(BoardXml board, set.boards) {
                if (board.name == item->text(0)) {
                    m_currentSet = set;
                    m_currentBoard = board;
                    return;
                }
            }
        }
    }
}

#include "selectboard.moc"
