#ifndef BOARDITEM_H
#define BOARDITEM_H

#include <QGraphicsItem>
#include "board.h"


class BoardItem : public QGraphicsItem
{
public:
    enum SelectionMode {
        ToggleTile,
        MarkAsFilledTile,
        MarkAsEmptyTile,
        NullTile
    };
public:
    BoardItem(QGraphicsItem *parent=0);

    void setBoard(Board *board);
    Board *board() const;

    QRectF boundingRect() const;

    void paint(QPainter *painter,
               const QStyleOptionGraphicsItem *option,
               QWidget *widget);

    SelectionMode selectionMode() const;
    void setSelectionMode(SelectionMode mode);
protected:
    void mousePressEvent(QGraphicsSceneMouseEvent *event);
    void mouseMoveEvent(QGraphicsSceneMouseEvent *event);
    void mouseReleaseEvent(QGraphicsSceneMouseEvent *event);
    void hoverEnterEvent(QGraphicsSceneHoverEvent *event);
    void hoverLeaveEvent(QGraphicsSceneHoverEvent *event);
    void hoverMoveEvent(QGraphicsSceneHoverEvent *event);
    QVariant itemChange(GraphicsItemChange change, const QVariant &value);
private:
    QSizeF computeSize() const;

    void drawBoard(QPainter *p);
    void drawHints(QPainter *p);
    void drawHighlight(QPainter *p);
    void drawTiles(QPainter *p);
    void drawFilled(QPainter *p, const QRectF &rect);
    void drawEmpty(QPainter *p, const QRectF &rect);
    void drawMultipleSelect(QPainter *p);

    int rowAt(qreal x) const;
    int columnAt(qreal y) const;

    QRectF rectAt(int column, int row) const;

    Board::TileState toggleState(Board::TileState state) const;
private:
    Board *m_board;
    int m_currentColumn;
    int m_currentRow;

    QPoint m_beginSelection;
    QPoint m_endSelection;

    SelectionMode m_selectionMode;
};

#endif
