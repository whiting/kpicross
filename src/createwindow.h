/**
 * createwindow.h
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C) 2008-2011 Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#ifndef CREATEWINDOW_H
#define CREATEWINDOW_H


#include <kdialog.h>

class Board;

namespace Ui
{
    class CreateWindowWidget;
}

class CreateWindow : public KDialog
{
    Q_OBJECT
public:
    CreateWindow(QWidget *parent = 0);

    void changeBoard( Board *board );

public slots:
    void saveBoard();
    void slotTitleChanged( const QString& title );
    void slotChangeSize( int i );

private:
    Ui::CreateWindowWidget *m_mainWidget;
};

#endif
