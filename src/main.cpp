/**
 * main.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C)  2007  Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#include "mainwindow.h"

#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>
#include <kapplication.h>

int main( int argc, char **argv )
{
    KAboutData about( "kpicross", 0, ki18n( "KPicross" ), "0.1", ki18n("A Picross game"),
        KAboutData::License_GPL, ki18n("© 2004 Zack Rusin\n© 2007 Jeremy Whiting"));
    about.addAuthor( ki18n("Zack Rusin"), ki18n("Plane-hater"), "zack@kde.org" );
    about.addAuthor( ki18n("Jeremy Whiting"), ki18n("Port to KDE 4"), "jpwhiting@kde.org" );
    KCmdLineArgs::init( argc, argv, &about );
    KApplication app;

    MainWindow *mainWin = new MainWindow( 0 );
    mainWin->show();
    app.setTopWidget( mainWin );
    return app.exec();
}
