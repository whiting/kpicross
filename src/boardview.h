/**
 * boardview.h
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C)  2007-2011  Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#ifndef BOARDVIEW_H
#define BOARDVIEW_H

#include "board.h"

#include <QGraphicsView>

class BoardItem;

class BoardView : public QGraphicsView
{
    Q_OBJECT
public:
    BoardView(QWidget *parent=0);
    ~BoardView();

    Board *board() const;
signals:
    void done();

public slots:
    void setBoard(Board *board);

private slots:
    /**
     * Slot to react to newBoard signal from the board.
     */
    void boardChanged();

private:
    BoardItem *m_boardItem;
    Board *m_board;
};

#endif
