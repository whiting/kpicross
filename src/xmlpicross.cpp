/**
 * xmlpicross.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "xmlpicross.h"

#include <QFile>
#include <QXmlStreamReader>

#include <QDebug>

QList<BoardSetXml> XmlPicross::parseFile(const QString &fname)
{
    QFile f(fname);
    f.open(QIODevice::ReadOnly);

    QXmlStreamReader xml(&f);
    QList<BoardSetXml> sets;
    BoardSetXml set;
    BoardXml board;

    while (!xml.atEnd()) {
        switch (xml.readNext()) {
        case QXmlStreamReader::StartElement: {
            QString name = xml.name().toString();
            QXmlStreamAttributes attrs = xml.attributes();
            qDebug()<<"Start = "<<name;
            if (name == QLatin1String("set")) {
                set = BoardSetXml();
                set.name = attrs.value("name").toString();
            } else if (name == QLatin1String("board")) {
                board = BoardXml();
                board.name = attrs.value("name").toString();
                board.author = attrs.value("author").toString();
                board.rows = attrs.value("rows").toString().toInt();
                board.columns = attrs.value("columns").toString().toInt();
            } else if (name == QLatin1String("image")) {
                board.imageType = attrs.value("type").toString();
                board.imageLink = attrs.value("link").toString();
            } else if (name == QLatin1String("data")) {
                QVector< QVector<bool> > data(board.rows,
                                              QVector<bool>(board.columns, false));
                QString text = xml.readElementText();
                QList<int> nums;
                QString::const_iterator itr;
                qDebug()<<"text len = "<<text.length();
                for (itr = text.constBegin(); itr != text.constEnd(); ++itr) {
                    const QChar &c = *itr;
                    if (c.isDigit()) {
                        int i = c.digitValue();
                        nums.append(i);
                    }
                }
                int idx = 0;
                qDebug()<<"iteratir over "<<board.rows<<" and "<<board.columns
                        <<", num of nums = "<<nums.count();
                for (int i = 0; i < board.rows; ++i) {
                    for (int j = 0; j < board.columns; ++j) {
                        data[i][j] = static_cast<bool>(nums[idx]);
                        ++idx;
                    }
                }
                board.data = data;
            }
        }
            break;
        case QXmlStreamReader::EndElement: {
            QString name = xml.name().toString();
            qDebug()<<"End = "<<name;
            if (name == QLatin1String("set")) {
                sets.append(set);
            } else if (name == QLatin1String("board")) {
                set.boards.append(board);
            }
        }
            break;
        case QXmlStreamReader::Characters:
        default:
            break;
        }
    }
    if (xml.hasError()) {
        // do error handling
        qDebug()<<"Error: "<<xml.error();
    }

    return sets;
}
