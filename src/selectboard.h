/**
 * selectboard.h
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */

#ifndef SELECTBOARD_H
#define SELECTBOARD_H

#include "xmlpicross.h"
#include <kdialog.h>

#include "ui_selectlevelui.h"

class Board;
class Levels;
class QTreeWidgetItem;

class SelectBoard : public KDialog
{
    Q_OBJECT
public:
    SelectBoard(const QList<BoardSetXml> &sets, QWidget *parent);

    BoardXml selectedBoard() const;

protected slots:
    void boardActivated(QTreeWidgetItem *item, int column);

protected:
    void fillTreeView();
    void findBoardFromItem(const QTreeWidgetItem *item) const;

private:
    Ui::LevelSelectUi *m_ui;
    QList<BoardSetXml> m_sets;

    mutable BoardSetXml m_currentSet;
    mutable BoardXml m_currentBoard;
};

#endif
