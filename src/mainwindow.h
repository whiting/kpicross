/**
 * mainwindow.h
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "xmlpicross.h"

#include <KXmlGuiWindow>
#include <QtCore/QPointer>

class BoardView;
class Board;
class QWidget;
class CreateWindow;

class MainWindow : public KXmlGuiWindow
{
    Q_OBJECT
public:
    MainWindow( QWidget *parent );

    void changeBoard( Board *board );

protected slots:
    void slotPickDifferent();
    void slotCreate();
    void slotDone();

protected:
    void initActions();
    void loadDefaultLevels();
    Board *defaultBoard();
private:
    QList<BoardSetXml> m_boardSets;
    BoardView *m_view;
    QPointer<CreateWindow> m_createWindow;
};

#endif
