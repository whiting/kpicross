/**
 * boardview.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C)  2011  Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "boardview.h"

#include "boarditem.h"

BoardView::BoardView(QWidget *parent)
    :QGraphicsView(parent),
     m_board(new Board)
{
    setScene(new QGraphicsScene);
    connect(m_board, SIGNAL(completed()), SIGNAL(done()));
    connect(m_board, SIGNAL(newBoard()), SLOT(boardChanged()));

    m_boardItem = new BoardItem;
    m_boardItem->setBoard(m_board);
    scene()->addItem(m_boardItem);
}

BoardView::~BoardView()
{
}

void BoardView::setBoard(Board *board)
{
    if (board && board != m_board) {
        disconnect(m_board, SIGNAL(completed()), this, SIGNAL(done()));
        disconnect(m_board, SIGNAL(newBoard()), this, SLOT(boardChanged()));

        m_board = board;
        m_boardItem->setBoard(m_board);
        connect(m_board, SIGNAL(completed()), SIGNAL(done()));
        connect(m_board, SIGNAL(newBoard()), SLOT(boardChanged()));
    }
}

void BoardView::boardChanged()
{
    m_boardItem->setBoard(m_board);
}

Board *BoardView::board() const
{
    return m_board;
}

#include "boardview.moc"
