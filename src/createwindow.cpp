/**
 * createwindow.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C) 2008 Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "createwindow.h"

#include "boardview.h"
#include "board.h"
#include "ui_createwindow.h"

#include <klineedit.h>
#include <kpushbutton.h>
#include <klocale.h>
#include <kglobal.h>
#include <kstandarddirs.h>
#include <kdebug.h>

#include <qbuttongroup.h>
#include <qlayout.h>
#include <qfile.h>
#include <qdatastream.h>
#include <qvector.h>

CreateWindow::CreateWindow( QWidget *parent )
    : KDialog(parent)
{
    QWidget *widget = new QWidget(this);
    m_mainWidget = new Ui::CreateWindowWidget;
    m_mainWidget->setupUi(widget);
    setMainWidget(widget);
    setCaption(i18n("Picross Board Editor"));

    connect( m_mainWidget->titleLineEdit, SIGNAL(textChanged(const QString&)),
             SLOT(slotTitleChanged(const QString&)) );
    QButtonGroup * sizeGroup = new QButtonGroup(this);
    sizeGroup->addButton(m_mainWidget->radioButton5, 5);
    sizeGroup->addButton(m_mainWidget->radioButton10, 10);
    sizeGroup->addButton(m_mainWidget->radioButton15, 15);
    sizeGroup->addButton(m_mainWidget->radioButton20, 20);
    connect( sizeGroup, SIGNAL(buttonClicked(int)), SLOT(slotChangeSize(int)) );

    QVector< QVector<bool> > pic( 20, QVector<bool>( 20, false ) );
    BoardXml xml;
    xml.name = "new board";
    Board *b = new Board(xml, this);
    b->setPicture( pic );
    changeBoard(b);
}

void CreateWindow::changeBoard( Board *board )
{
    m_mainWidget->boardView->setBoard( board );
    m_mainWidget->titleLineEdit->setText( board->data().name );
}


void CreateWindow::slotTitleChanged( const QString& title )
{
    m_mainWidget->boardView->board()->setTitle( title );
}

void CreateWindow::saveBoard()
{
    Board *board = m_mainWidget->boardView->board();
    QString filePath = KStandardDirs::locateLocal( "appdata",
                                                   QString( "%1.pic" ).arg(
                                                       board->data().name));

    QVector< QVector<Board::TileState> > view = board->board();
    QVector< QVector<bool> > pic( board->rows(), QVector<bool>( board->columns(), false ) );

    for ( int i = 0; i < board->rows(); ++i ) {
        for ( int j = 0; j < board->columns(); ++j ) {
            if ( view[i][j] == Board::CheckedAsFilled )
                pic[i][j] = true;
        }
    }
    BoardXml xml; xml.name = board->data().name;
    Board *saveBoard = new Board(xml, this);
    saveBoard->setPicture( pic );

    kDebug()<<"Saving at "<<filePath<<endl;
    QFile file( filePath );
    file.open( QFile::WriteOnly );
    QDataStream str( &file );
    saveBoard->save( str );
    file.close();
    delete saveBoard;
}

void CreateWindow::slotChangeSize( int size )
{
    if ( m_mainWidget->boardView->board()->rows() != size ) {
        QVector< QVector<bool> > pic( size, QVector<bool>( size, false ) );
        m_mainWidget->boardView->board()->setPicture( pic );
    }
}


#include "createwindow.moc"
