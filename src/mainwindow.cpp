/**
 * mainwindow.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 * Copyright (C) 2008-2011 Jeremy Whiting <jpwhiting@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "mainwindow.h"
#include "selectboard.h"

#include "boardview.h"
#include "createwindow.h"

#include <KAction>
#include <KActionCollection>
#include <KMessageBox>
#include <KStatusBar>
#include <KStandardAction>

#include <kdebug.h>

const QString congratsText = i18n( "You completed the puzzle correctly!\n%1 by %2" );

MainWindow::MainWindow( QWidget *parent )
    : KXmlGuiWindow(parent), m_view(new BoardView), m_createWindow(NULL)
{
    loadDefaultLevels();
    initActions();

    m_view->setBoard(defaultBoard());
    connect(m_view, SIGNAL(done()), SLOT(slotDone()));

    setCentralWidget(m_view);

    setupGUI(Default, "kpicross.rc");
}

void MainWindow::changeBoard(Board *board)
{
    if (board) {
        m_view->setBoard(board);
    }
}

void MainWindow::slotCreate()
{
    if (m_createWindow == NULL)
    {
        m_createWindow = new CreateWindow(this);
        //connect(m_createWindow, SIGNAL(boardSaved()), m_levels, SLOT(load()));
    }
    m_createWindow->show();
}

void MainWindow::initActions()
{
    KStandardAction::quit( this, SLOT(close()), actionCollection() );

    //new KAction( i18n("&Check"), "check", 0,
    //this, SLOT(slotCheckBoard()), actionCollection(), "picross_check" );
    KAction * nextAction = new KAction( i18n("&Choose Board..."), actionCollection());
    connect(nextAction, SIGNAL(triggered()), this, SLOT(slotPickDifferent()));
    actionCollection()->addAction("picross_pick", nextAction);

    KAction * createAction = new KAction( i18n("&Create Board..."), actionCollection());
    connect(createAction, SIGNAL(triggered()), this, SLOT(slotCreate()));
    actionCollection()->addAction("picross_create", createAction);
}

void MainWindow::slotDone()
{
    Board * board = m_view->board();
    KMessageBox::information(this,
        congratsText.arg(board->title()).arg(board->author()));
}

void MainWindow::slotPickDifferent()
{
    SelectBoard *select = new SelectBoard(m_boardSets, this);

    if (select->exec() == QDialog::Accepted) {
        BoardXml board = select->selectedBoard();
        changeBoard(new Board(board, this));
        return;
    }
}

void MainWindow::loadDefaultLevels()
{
    QList<BoardSetXml> basic = XmlPicross::parseFile(":/basics.xml");

    qDebug()<<"Basic sets = "<<basic.count();
    if (basic.count())
        qDebug()<<"\tnum boards = "<<basic.first().boards.count();

    m_boardSets += basic;
}

Board * MainWindow::defaultBoard()
{
    BoardSetXml set = m_boardSets.first();
    return new Board(set.boards.first());
}

#include "mainwindow.moc"
