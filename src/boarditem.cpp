#include "boarditem.h"
#include "board.h"

#include <QSvgRenderer>
#include <QGraphicsSceneMouseEvent>
#include <QPainter>
#include <QBrush>
#include <QFont>
#include <QFontMetrics>
#include <QDebug>
#include <math.h>

const float MARGIN_WIDTH      = 1.0;
const float BOLD_MARGIN_WIDTH = 2.0;
const float CELL_SIZE         = 20.0;



BoardItem::BoardItem(QGraphicsItem *parent)
    : QGraphicsItem(parent),
      m_board(0),
      m_currentColumn(-1),
      m_currentRow(-1),
      m_beginSelection(-1, -1),
      m_endSelection(-1, -1),
      m_selectionMode(ToggleTile)
{
    setAcceptHoverEvents(true);
}

QRectF BoardItem::boundingRect() const
{
    if (m_board) {
        QSizeF size = computeSize();

        return QRectF(0, 0, size.width(), size.height());
    } else {
        return QRectF(0, 0, 300, 300);
    }
}

void BoardItem::paint(QPainter *painter,
                      const QStyleOptionGraphicsItem *option,
                      QWidget *widget)
{
    Q_UNUSED(widget);
    Q_UNUSED(option);

    drawHighlight(painter);
    drawBoard(painter);
    drawHints(painter);
    drawTiles(painter);
    drawMultipleSelect(painter);

    if (m_board->isCompleted()) {
        BoardXml xml = m_board->data();
        if (!xml.imageLink.isEmpty()) {
            QImage img(xml.imageLink);
            QSvgRenderer renderer(xml.imageLink);
            QRectF start = rectAt(0, 0);
            QRectF end = rectAt(m_board->columns() - 1, m_board->rows() - 1);
            QRectF full = QRectF(start.x(), start.y(),
                                 end.right() - start.x(), end.bottom() - start.y());
            renderer.render(painter, full);
        }
    }
}

void BoardItem::setBoard(Board *b)
{
    m_board = b;
    setEnabled(true);
    prepareGeometryChange();
    update();
}

Board * BoardItem::board() const
{
    return m_board;
}

QSizeF BoardItem::computeSize() const
{
    int rows = m_board->rows();
    int cols = m_board->columns();
    int boldMarginsRows = rows / 5;
    int boldMarginsCols = cols / 5;
    int marginRows = rows - boldMarginsRows;
    int marginCols = cols - boldMarginsCols;

    boldMarginsRows += (m_board->maxRowHints());
    boldMarginsCols += (m_board->maxColumnHints());
    rows += m_board->maxRowHints();
    cols += m_board->maxColumnHints();

    float width = rows * CELL_SIZE +
                  boldMarginsRows * BOLD_MARGIN_WIDTH +
                  marginRows * MARGIN_WIDTH;

    float height = cols * CELL_SIZE +
                   boldMarginsCols * BOLD_MARGIN_WIDTH +
                   marginCols * MARGIN_WIDTH;

    return QSizeF(width, height);
}

void BoardItem::drawBoard(QPainter *painter)
{
    float x, y;
    QPen pen(QBrush(Qt::black), MARGIN_WIDTH);
    QPen boldPen(QBrush(Qt::black), BOLD_MARGIN_WIDTH);
    painter->save();
    painter->setPen(boldPen);

    QSizeF size = computeSize();

    x = 0;
    y = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxColumnHints();
    for (int i = 0; i <= m_board->rows(); ++i) {
        bool bold = false;
        bool willBeBold = false;

        bold = !(i % 5);
        willBeBold = !((i + 1) % 5);

        if (bold) {
            painter->setPen(boldPen);
            painter->drawLine(x, y, size.width(), y);
        } else {
            painter->setPen(pen);
            painter->drawLine(x, y, size.width(), y);
        }
        if (willBeBold)
            y += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            y += CELL_SIZE + MARGIN_WIDTH;
    }

    x = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxRowHints();
    y = 0;
    for (int i = 0; i <= m_board->columns(); ++i) {
        bool bold = false;
        bool willBeBold = false;

        bold = !(i % 5);
        willBeBold = !((i + 1) % 5);

        if (bold) {
            painter->setPen(boldPen);
            painter->drawLine(x, y, x, size.height());
        } else {
            painter->setPen(pen);
            painter->drawLine(x, y, x, size.height());
        }
        if (willBeBold)
            x += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            x += CELL_SIZE + MARGIN_WIDTH;
    }
    painter->restore();
}

static inline QList<int> reversedList(const QList<int> &lst)
{
    QList<int> ret;
    QListIterator<int> itr(lst);
    itr.toBack();

    while (itr.hasPrevious())
        ret.append(itr.previous());

    return ret;
}

static QVector<QPair<int, bool> > computeCompleted(const QList<int> &hints,
                                   const QVector<Board::TileState> &vec)
{
    QVector<QPair<int, bool> > res(hints.count());
    QPair<int, bool> pair;
    QList<int> foundHints;
    int current = 0;
    bool allEmpty = true;
    int checkedUntil = hints.count();

    for (int i = 0; i < hints.count(); ++i) {
        pair.first = hints[i];
        pair.second = false;
        res[i] = pair;
    }

    for (int i = 0; i < vec.count(); ++i) {
        if (current &&
            vec[i] != Board::CheckedAsFilled) {
            foundHints.append(current);
            current = 0;
        }
        if (vec[i] == Board::CheckedAsFilled) {
            allEmpty = false;
            ++current;
        } else if (vec[i] == Board::CheckedAsEmpty) {
            current = 0;
        } else {
            checkedUntil = qMin(foundHints.count(), checkedUntil);
            allEmpty = false;
            current = 0;
        }
    }
    if (current)
        foundHints.append(current);
    if (allEmpty) {
        foundHints.append(0);
    }
    for (int i = 0; i < foundHints.count() && i < hints.count(); ++i) {
        if (foundHints[i] == hints[i]
            && (checkedUntil > i || foundHints.count() == hints.count())
            && foundHints.count() <= hints.count()) {
            pair.first = foundHints[i];
            pair.second = true;
            res[i] = pair;
        } else
            break;
    }

    //now from the back
    current = 0;
    allEmpty = true;
    checkedUntil = hints.count();
    foundHints.clear();
    for (int i = vec.count() - 1; i >= 0; --i) {
        if (current &&
            vec[i] != Board::CheckedAsFilled) {
            foundHints.prepend(current);
            current = 0;
        } else if (vec[i] == Board::CheckedAsFilled) {
            allEmpty = false;
            ++current;
        } else if (vec[i] == Board::CheckedAsEmpty) {
            current = 0;
        } else {
            checkedUntil = qMin(foundHints.count(), checkedUntil);
            allEmpty = false;
            current = 0;
        }
    }
    if (current)
        foundHints.prepend(current);
    if (allEmpty) {
        foundHints.append(0);
    }


    foundHints = reversedList(foundHints);
    QList<int> revHints = reversedList(hints);

    for (int i = 0; i < foundHints.count() && i < revHints.count(); ++i) {
#if 0
        qDebug()<<"At "<<i<<", its "<<foundHints[i]
                <<" == "<<revHints[i]
                << ", " << checkedUntil;
#endif
        if (foundHints[i] == revHints[i]
            && (checkedUntil > i || foundHints.count() == revHints.count())
            && foundHints.count() <= revHints.count()) {
            pair.first = foundHints[i];
            pair.second = true;
            res[revHints.count() - i - 1] = pair;
        } else
            break;
    }

    return res;
}

void BoardItem::drawHints(QPainter *painter)
{
    float x, y;
    QFont font("Andale Mono", 10);
    QFontMetricsF fm(font);
    int maxHints = m_board->maxRowHints();
    QRectF bounds = fm.tightBoundingRect(QString::fromLatin1("6"));
    x = 0;
    y = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxColumnHints();
    painter->setFont(font);
    QVector<QPair<int, bool> > completedState;

    for (int i = 0; i < m_board->rows(); ++i) {
        bool bold = false;
        QVector<Board::TileState> currentState =
            m_board->row(i);

        bold = !(i % 5);
        QList<int> hints = m_board->hintRow(i);
        completedState = computeCompleted(hints, currentState);
        float startX = (maxHints - hints.count()) *
                       (CELL_SIZE + BOLD_MARGIN_WIDTH);
        for (int j = 0; j < hints.count(); ++j) {
            int hint = hints[j];
            bool complete = completedState[j].second;
            x = startX + j * (CELL_SIZE + BOLD_MARGIN_WIDTH);
            float offset = 0;
            if (bold)
                offset += floor(BOLD_MARGIN_WIDTH/2);
            else
                offset += floor(MARGIN_WIDTH/2);

            QRectF rect(x, y + offset, CELL_SIZE, CELL_SIZE);
#if DEBUG_CELLS
            {
                painter->save();
                painter->setPen(QPen(Qt::NoPen));
                painter->setBrush(Qt::red);
                painter->drawRect(rect);
                painter->restore();
            }
#endif
            if (complete) {
                painter->save();
                QPen pen = painter->pen();
                pen.setColor(QColor(193, 193, 193));
                painter->setPen(pen);
                painter->drawText(rect, Qt::AlignCenter,
                                  QString("%1").arg(hint));
                painter->restore();
            } else
                painter->drawText(rect, Qt::AlignCenter,
                                  QString("%1").arg(hint));
        }
        if (bold)
            y += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            y += CELL_SIZE + MARGIN_WIDTH;
    }

    maxHints = m_board->maxColumnHints();
    x = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxRowHints() + MARGIN_WIDTH;
    for (int i = 0; i < m_board->columns(); ++i) {
        bool bold = false;
        QVector<Board::TileState> currentState =
            m_board->column(i);

        bold = !(i % 5);
        QList<int> hints = m_board->hintColumn(i);
        completedState = computeCompleted(hints, currentState);
        float startY = (maxHints - hints.count()) *
                       (CELL_SIZE + BOLD_MARGIN_WIDTH);
        for (int j = 0; j < hints.count(); ++j) {
            int hint = hints[j];
            bool complete = completedState[j].second;
            float offset = 0;
            if (bold)
                offset = floorf(MARGIN_WIDTH/2.f);
            else
                offset = floorf(BOLD_MARGIN_WIDTH/2.f);
            y = startY + j * (CELL_SIZE + BOLD_MARGIN_WIDTH);

            QRectF rect(x - offset, y, CELL_SIZE, CELL_SIZE);
#if DEBUG_CELLS
            {
                painter->save();
                painter->setPen(QPen(Qt::NoPen));
                painter->setBrush(Qt::red);
                painter->drawRect(rect);
                painter->restore();
            }
#endif
            if (complete) {
                painter->save();
                QPen pen = painter->pen();
                pen.setColor(QColor(193, 193, 193));
                painter->setPen(pen);
                painter->drawText(rect, Qt::AlignCenter,
                                  QString("%1").arg(hint));
                painter->restore();
            } else
                painter->drawText(rect, Qt::AlignCenter,
                                  QString("%1").arg(hint));
        }
        if (bold)
            x += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            x += CELL_SIZE + MARGIN_WIDTH;
    }

#if 0
    {
        painter->save();
        painter->setPen(QPen(Qt::NoPen));
        painter->setBrush(Qt::red);
        painter->drawRect(rectAt(11, 11));
        painter->restore();
    }
#endif
}

void BoardItem::mousePressEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF pt = event->pos();
    int col = columnAt(pt.x());
    int row = rowAt(pt.y());
    m_beginSelection = QPoint(col, row);
    m_endSelection = QPoint(col, row);
    if (col < 0 || row < 0)
        return;

    //reset the hover
    m_currentRow = -1;
    m_currentColumn = -1;

    update();
}

int BoardItem::rowAt(qreal y) const
{
    qreal startY = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxColumnHints() +
                   BOLD_MARGIN_WIDTH;
    if (y < startY)
        return  -1;
    y -= startY;
    int current = 0;
    qreal currentY = 0;

    while (current < m_board->rows()) {
        bool willBeBold = !((current + 1) % 5);
        qreal height;
        if (willBeBold)
            height = CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            height = CELL_SIZE + MARGIN_WIDTH;
        if ((currentY < y || qFuzzyCompare(currentY, y)) &&
            (currentY + height) > y)
            break;
        currentY += height;
        ++current;
    }
    if (current >= m_board->rows())
        return -1;

    return current;
}

int BoardItem::columnAt(qreal x) const
{
    qreal startX = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxRowHints() + MARGIN_WIDTH;
    if (x < startX)
        return  -1;
    x -= startX;
    int current = 0;
    qreal currentX = 0;

    while (current < m_board->columns()) {
        bool willBeBold = !((current + 1) % 5);
        qreal width;
        if (willBeBold)
            width = CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            width = CELL_SIZE + MARGIN_WIDTH;
        if ((currentX < x || qFuzzyCompare(currentX, x)) &&
            (currentX + width) > x)
            break;
        currentX += width;
        ++current;
    }
    if (current >= m_board->columns())
        return -1;

    return current;
}

QRectF BoardItem::rectAt(int column, int row) const
{
    qreal x, y;
    qreal startX = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxRowHints() + MARGIN_WIDTH;
    qreal startY = (CELL_SIZE + BOLD_MARGIN_WIDTH) * m_board->maxColumnHints() +
                   MARGIN_WIDTH;

    x = startX;
    y = startY;

    for (int i = 0; i < column; ++i) {
        bool willBeBold = !((i + 1) % 5);
        if (willBeBold)
            x += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            x += CELL_SIZE + MARGIN_WIDTH;
    }
    for (int i = 0; i < row; ++i) {
        bool willBeBold = !((i + 1) % 5);
        if (willBeBold)
            y += CELL_SIZE + BOLD_MARGIN_WIDTH;
        else
            y += CELL_SIZE + MARGIN_WIDTH;
    }

    return QRectF(x, y, CELL_SIZE, CELL_SIZE);
}

void BoardItem::drawHighlight(QPainter *painter)
{
    if (m_currentColumn < 0 || m_currentRow < 0)
        return;
    QRectF rectX = rectAt(m_currentColumn,
                          m_currentRow);
    QSizeF size = computeSize();
    {
        painter->save();
        painter->setPen(QPen(Qt::NoPen));
        painter->setBrush(QColor(170, 170, 170, 96));
        QRectF rect(0, rectX.y(),
                    size.width(), rectX.height());
        painter->drawRect(rect);
        rect = QRectF(rectX.x(), 0,
                      rectX.height(), size.height());
        painter->drawRect(rect);
        painter->restore();
    }
}

void BoardItem::hoverEnterEvent(QGraphicsSceneHoverEvent *)
{
}

void BoardItem::hoverLeaveEvent(QGraphicsSceneHoverEvent *)
{
    m_currentRow    = -1;
    m_currentColumn = -1;
}

void BoardItem::hoverMoveEvent(QGraphicsSceneHoverEvent *event)
{
    QPointF pt = event->pos();
    bool dirty = false;

    int index = columnAt(pt.x());
    if (index != m_currentColumn) {
        m_currentColumn = index;
        dirty = true;
    }

    index = rowAt(pt.y());
    if (index != m_currentRow) {
        m_currentRow = index;
        dirty = true;
    }

    if (dirty) {
        update();
    }
}

QVariant BoardItem::itemChange(GraphicsItemChange change, const QVariant &value)
{
    //FIXME: the board itself needs to be cached!

    return QGraphicsItem::itemChange(change, value);
}

void BoardItem::drawTiles(QPainter *p)
{
    for (int i = 0; i < m_board->rows(); ++i) {
        for (int j = 0; j < m_board->columns(); ++j) {
            QRectF rect = rectAt(j, i);
            Board::TileState state = m_board->tileAt(j, i);
            if (state == Board::CheckedAsFilled)
                drawFilled(p, rect);
            else if (state == Board::CheckedAsEmpty)
                drawEmpty(p, rect);
        }
    }
}

void BoardItem::drawFilled(QPainter *p, const QRectF &rect)
{
    p->fillRect(rect, Qt::black);
}

void BoardItem::drawEmpty(QPainter *p, const QRectF &rect)
{
    p->drawLine(rect.x(), rect.y(),
                rect.x() + rect.width() - 1,
                rect.y() + rect.height() - 1);
    p->drawLine(rect.x(), rect.y() + rect.height() - 1,
                rect.x() + rect.width() - 1,
                rect.y());
}

void BoardItem::mouseMoveEvent(QGraphicsSceneMouseEvent *event)
{
    QPointF pt = event->pos();
    int col = columnAt(pt.x());
    int row = rowAt(pt.y());
    m_endSelection = QPoint(col, row);
    if (col < 0 || row < 0)
        return;
    update();
}

void BoardItem::mouseReleaseEvent(QGraphicsSceneMouseEvent *event)
{
    if (m_beginSelection.x() < 0 ||
        m_beginSelection.y() < 0 ||
        m_endSelection.x() < 0 ||
        m_endSelection.y() < 0)
        return;

    SelectionMode oldMode = m_selectionMode;
    if (event->button() == Qt::RightButton)
        m_selectionMode = MarkAsEmptyTile;

    //do the selection only if it's vertical or horizontal
    if (m_beginSelection.x() == m_endSelection.x()) {
        int min = qMin(m_beginSelection.y(), m_endSelection.y());
        int max = qMax(m_beginSelection.y(), m_endSelection.y());

        for (int i = min; i <= max; ++i) {
            Board::TileState state = m_board->tileAt(m_beginSelection.x(), i);
            m_board->setTile(toggleState(state),
                             m_beginSelection.x(), i);
        }
        update();
    } else if (m_beginSelection.y() == m_endSelection.y()) {
        int min = qMin(m_beginSelection.x(), m_endSelection.x());
        int max = qMax(m_beginSelection.x(), m_endSelection.x());

        for (int i = min; i <= max; ++i) {
            Board::TileState state = m_board->tileAt(i, m_beginSelection.y());
            m_board->setTile(toggleState(state),
                             i, m_beginSelection.y());
        }
        update();
    }
    m_beginSelection = QPoint(-1, -1);
    m_endSelection = QPoint(-1, -1);
    if (event->button() == Qt::RightButton)
        m_selectionMode = oldMode;
}

Board::TileState BoardItem::toggleState(Board::TileState state) const
{
    if (m_selectionMode == MarkAsFilledTile)
        return Board::CheckedAsEmpty;
    else if (m_selectionMode == MarkAsEmptyTile)
        return Board::CheckedAsEmpty;
    else if (m_selectionMode == NullTile)
        return Board::Empty;

    switch(state) {
    case Board::CheckedAsFilled:
        return Board::CheckedAsEmpty;
        break;
    case Board::CheckedAsEmpty:
        return Board::Empty;
        break;
    case Board::Empty:
        return Board::CheckedAsFilled;
        break;
    }

    return Board::Empty;
}

void BoardItem::drawMultipleSelect(QPainter *painter)
{
    if (m_beginSelection.x() < 0 ||
        m_beginSelection.y() < 0 ||
        m_endSelection.x() < 0 ||
        m_endSelection.y() < 0)
        return;
    if (m_beginSelection.x() == m_endSelection.x()) {
        QRectF startRect = rectAt(m_beginSelection.x(),
                                  m_beginSelection.y());
        QRectF endRect = rectAt(m_endSelection.x(),
                                m_endSelection.y());
        QRectF realRect(startRect.x(), qMin(startRect.y(), endRect.y()),
                        startRect.width(),
                        qMax(startRect.y() + startRect.height(),
                             endRect.y() + endRect.height())
                        -
                        qMin(startRect.y(), endRect.y()));
        {
            painter->save();
            painter->setPen(QPen(Qt::NoPen));
            painter->setBrush(QColor(170, 170, 170, 96));
            painter->drawRect(realRect);
            painter->restore();
        }
    } else if (m_beginSelection.y() == m_endSelection.y()) {
        QRectF startRect = rectAt(m_beginSelection.x(),
                                  m_beginSelection.y());
        QRectF endRect = rectAt(m_endSelection.x(),
                                m_endSelection.y());
        QRect realRect(qMin(startRect.x(), endRect.x()), startRect.y(),
                       qMax(startRect.x() + startRect.width(),
                            endRect.x() + endRect.width())
                       -
                       qMin(startRect.x(), endRect.x()),
                       startRect.height());
        {
            painter->save();
            painter->setPen(QPen(Qt::NoPen));
            painter->setBrush(QColor(170, 170, 170, 96));
            painter->drawRect(realRect);
            painter->restore();
        }
    }
}

BoardItem::SelectionMode BoardItem::selectionMode() const
{
    return m_selectionMode;
}

void BoardItem::setSelectionMode(SelectionMode mode)
{
    m_selectionMode = mode;
}
