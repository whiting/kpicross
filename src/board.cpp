/**
 * board.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "board.h"

#include <qdebug.h>

Board::Board(QObject *parent)
    : QObject(parent),
      m_maxRowHints(0),
      m_maxColumnHints(0),
      m_tilesLeft(0)
{
}

Board::Board(const BoardXml &data, QObject *parent)
    : QObject(parent),
      m_data(data),
      m_maxRowHints(0),
      m_maxColumnHints(0),
      m_tilesLeft(0)
{
    if (!m_data.data.isEmpty())
        setPicture(m_data.data);
    qDebug()<<"Setting board "<<m_data.name
            <<", rows = "<<m_data.rows
            <<", cols = "<<m_data.columns;
}

int Board::rows() const
{
    return m_hintRows.count();
}

int Board::columns() const
{
    return m_hintColumns.count();
}

QList<int> Board::hintRow(int i) const
{
    return m_hintRows[i];
}

QList<int> Board::hintColumn(int i) const
{
    return m_hintColumns[i];
}

QVector< QVector<Board::TileState> > Board::board() const
{
    return m_board;
}

QVector<Board::TileState> Board::row(int i) const
{
    return m_board[i];
}

bool Board::isCompleted() const
{
    return !m_tilesLeft;
}

void Board::setTile(TileState state, int x, int y)
{
    // first see if the tile at y, x isn't already in state state
    if (m_board[y][x] != state) {
        if (m_data.data[y][x]) {
            if (state == CheckedAsFilled) {
                --m_tilesLeft;
            } else if (state == CheckedAsEmpty || state == Empty) {
                if (m_board[y][x] == CheckedAsFilled)
                    ++m_tilesLeft;
            }
        } else {
            if (state == CheckedAsFilled) { //wrongly checked
                if (m_board[y][x] != CheckedAsFilled) //hasn't been checked already
                    ++m_tilesLeft;
            } else if (m_board[y][x] == CheckedAsFilled) // has been wrongly checked
                --m_tilesLeft;
        }

        m_board[y][x] = state;
        emit changed(state, x, y);
        //qDebug()<<"Incorrect "<< m_tilesLeft<<endl;

        if (!m_tilesLeft)
            emit completed();
    }
}

void Board::setPicture(const QVector< QVector<bool> > &pic)
{
    m_data.data = pic;
    m_board = QVector< QVector<TileState> >(
        pic.count(),
        QVector<TileState>(pic[0].count(), Board::Empty));
    computeHints();
    emit newBoard();
}

void Board::computeHints()
{
    computeHintRows();
    computeHintColumns();
}

void Board::computeHintRows()
{
    m_hintRows = QVector< QList<int> >(m_board.count());

    for (int row = 0; row < m_data.data.count(); ++row) {
        QVector<bool> mrow = m_data.data[row];
        int consecutive = 0;
        bool found = false;
        int numHints = 0;
        for (int x = 0; x < mrow.count(); ++x) {
            if (mrow[x]) {
                ++m_tilesLeft;
                ++consecutive;
            } else if (consecutive) {
                ++numHints;
                found = true;
                m_hintRows[row].append(consecutive);
                consecutive = 0;
            }
        }

        if (consecutive) {
            //in case it was the last row
            ++numHints;
            found = true;
            m_hintRows[row].append(consecutive);
        }

        if (!found) {
            m_hintRows[row].append(0);
            numHints = 1;
        }

        if (numHints > m_maxRowHints) {
            m_maxRowHints = numHints;
        }
    }
}

void Board::computeHintColumns()
{
    m_hintColumns = QVector< QList<int> >(m_board[0].count());

    int height = m_data.data.count();
    int width  = m_data.data[0].count();

    for (int x = 0; x < width; ++x) {
        int consecutive = 0;
        bool found = false;
        int numHints = 0;
        for (int y = 0; y < height; ++y) {
            if (m_data.data[y][x]) {
                consecutive++;
            } else if (consecutive) {
                ++numHints;
                found = true;
                m_hintColumns[x].append(consecutive);
                consecutive = 0;
            }
        }
        if (consecutive) {
            //in case it was the last column
            found = true;
            ++numHints;
            m_hintColumns[x].append(consecutive);
        }

        if (!found) {
            m_hintColumns[x].append(0); //nothing in this columnd
            numHints = 1;
        }

        if (numHints > m_maxColumnHints) {
            m_maxColumnHints = numHints;
        }
    }
}

QPair<int, int> Board::giveHint() const
{
    int height = m_data.data.count();
    int width  = m_data.data[0].count();

    for (int y = 0; y < height; ++y) {
         for (int x = 0; x < width; ++x) {
             if (m_data.data[y][x]) {
                 if (m_board[y][x] != CheckedAsFilled)
                     return QPair<int, int>(x, y);
             }
         }
    }
    return QPair<int, int>(-1, -1);
}

Board::TileState Board::tileAt(int x, int y) const
{
    return m_board[y][x];
}

int Board::maxRowHints() const
{
    return m_maxRowHints;
}

int Board::maxColumnHints() const
{
    return m_maxColumnHints;
}

void Board::load(QDataStream &stream)
{
    QString str(400, 0);
    stream >> m_data.name >> m_data.rows >> m_data.columns;
    m_data.data =  QVector< QVector<bool> >(m_data.rows,
                                            QVector<bool>(
                                                m_data.columns, false));
    qDebug()<<"rows = "<<m_data.rows<<", cols = "<<m_data.columns;
    for (int i = 0; i < m_data.rows; ++i) {
        for (int j = 0; j < m_data.columns; ++j) {
            qint8 field;
            stream >> field;
            str += QString("%1").arg(field);
            m_data.data[i][j] = static_cast<bool>(field);
        }
    }
    qDebug()<<"str len = "<<str.length();
    qDebug()<<"XXXXXXXXX is \""<<str<<"\"";
    setPicture(m_data.data);
}

void Board::save(QDataStream &stream)
{
    int rows = m_data.data.count();
    int cols = m_data.data[0].count();
    stream << m_data.name << rows << cols;

    for (int i = 0; i < rows; ++i) {
        for (int j = 0; j < cols; ++j) {
            qint8 field = m_data.data[i][j];
            stream << field;
        }
    }
}

QString Board::title() const
{
    return m_data.name;
}

void Board::setTitle(const QString &title)
{
    m_data.name = title;
}

QString Board::author() const
{
    return m_data.author;
}

void Board::setAuthor(const QString &author)
{
    m_data.author = author;
}

QVector<Board::TileState> Board::column(int j) const
{
    QVector<TileState> res(rows());

    for (int i = 0; i < m_board.count(); ++i) {
        res[i] = m_board[i][j];
    }
    return res;
}

BoardXml Board::data() const
{
    return m_data;
}
