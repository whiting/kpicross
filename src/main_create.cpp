/**
 * main_create.cpp
 *
 * Copyright (C)  2004  Zack Rusin <zack@kde.org>
 *
 * This program is free software; you can redistribute it and/or
 * modify it under the terms of the GNU General Public License
 * as published by the Free Software Foundation; either version 2
 * of the License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA
 * 02111-1307, USA.
 */
#include "createwindow.h"
#include "boardview.h"
#include "board.h"

#include <kapplication.h>
#include <kaboutdata.h>
#include <kcmdlineargs.h>
#include <klocale.h>

#include <qvaluevector.h>

static const char *description =
    I18N_NOOP( "A Picross board creator" );

static const char *version = "0.1";

static KCmdLineOptions options[] =
{
    { 0, 0, 0 }
};

int main( int argc, char **argv )
{
    KAboutData about( "picross", I18N_NOOP( "Picross Creator" ), version, description,
                      KAboutData::License_GPL, "(C) 2004 Zack Rusin",
                      0, 0, "zack@kde.org" );
    about.addAuthor( "Zack Rusin", "Plane-hater", "zack@kde.org" );
    KCmdLineArgs::init( argc, argv, &about );
    KCmdLineArgs::addCmdLineOptions( options );
    KApplication app;


    KCmdLineArgs *args = KCmdLineArgs::parsedArgs();

    // TODO: do something with the command line args here
    CreateWindow *mainWin = new CreateWindow( 0 );
    app.setMainWidget( mainWin );
    mainWin->show();

    args->clear();

    int ret = app.exec();

    return ret;
}
